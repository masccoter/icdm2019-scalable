#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pickle
import numpy as np

data_prefix = './data/'

a = []
myFile = open(data_prefix + 'Pubmed-Diabetes.DIRECTED.cites.tab' , 'r' )
for Row in myFile:
    a.append(Row.split('\t'))
myFile.close()

a = a[2:]    # first two lines are descriptions
paper_pair = []

for idx in a:
    p1 = idx[1][6:]    # form: 'paper:' + id
    p2 = idx[3][6:-1]  # form: 'paper:' + id + '\n'
    paper_pair.append([p1, p2])


b = []
myFile= open(data_prefix + 'Pubmed-Diabetes.NODE.paper.tab', 'r' )
for Row in myFile:
    b.append(Row.split('\t'))
myFile.close()

description = b[1]
b = b[2:]       # first two lines are descriptions
label = {}
features = {}

feature_list = []
description = description[1:-1]
for k in description:
    k = k[8:]
    stop = k.index(':')
    k = k[:stop]
    feature_list.append(k)

for idx in b:
    paper_id = idx[0]
    truth = idx[1][-1]      # form: 'label=' + n
    label[paper_id] = int(truth) - 1 # convert from [1,2,3] to [0,1,2]
    
    # get features
    feature = np.zeros(len(feature_list))
    feature_description = idx[2:-1]
    for k in feature_description:
        start = k.index('=')
        feature_name = k[ : start]
        feature_value = float( k[start+1 : ] )
        feature_location = feature_list.index(feature_name)
        feature[feature_location] = feature_value
    features[paper_id] = feature
# write to file
with open(data_prefix + 'paper_pair', 'wb') as f:
    pickle.dump(paper_pair, f)

with open(data_prefix + 'ground_truth', 'wb') as f:
    pickle.dump(label, f)

with open(data_prefix + 'features', 'wb') as f:
    pickle.dump(features, f)
