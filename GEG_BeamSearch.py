#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pickle
import utils

def get_action_candidates(states):
    action_candidates_m = []
    for state in states:
        node_list = [step[1] for step in state]
        action_candidates = []
        for node in node_list:
            neighbors = list(messages[node].keys())
            for neighbor in neighbors:
                if (neighbor, node) not in state and (node, neighbor) not in state and neighbor not in node_list:
                    action_candidates.append((node, neighbor))
        action_candidates_m.append(action_candidates)
    return action_candidates_m

def find_beam_subgraph(cur_subgraph_m, cur_alter_m, center_id, num_beam):
    KL_lists = []
    subgraphs = []
    alters = []
    for it, cur_subgraph in enumerate(cur_subgraph_m):
        cur_alter = cur_alter_m[it]
        if len(cur_alter) == 0:
            continue
        for pair in cur_alter:
            cand_subgraph = cur_subgraph.copy()
            cand_subgraph.append(pair)
    
            model = utils.BP(cand_subgraph, priors, potentials, len(potentials))
            model.schedule()
            model.run_bp()
            cand_belief = model.classify()
            KL_lists.append( utils.sum_KL( cand_belief[center_id], beliefs[center_id]) )
            subgraphs.append(cur_subgraph)
            alters.append(pair)
    return_m = []

    sorted_rank = np.argsort(KL_lists)

    for it in sorted_rank:
        temp = subgraphs[it].copy()
        temp.append(alters[it])
        if temp not in return_m:
            return_m.append(temp)
            num_beam -= 1
        if num_beam == 0:
            break
    while num_beam:
        temp = subgraphs[0].copy()
        temp.append(alters[0])
        return_m.append(temp)
        num_beam -= 1
    return return_m

#ratio = '95'
num_beam = 3
k_class = 3
eyes = (1 - 0.1*k_class/(k_class-1) ) * np.diag(np.ones(k_class))
potentials = (eyes + 0.1/(k_class-1) * np.ones([k_class, k_class]))
max_len = 5
data_prefix = './data/'
ratios = ['5','10','20','30','40','50','60','70','80','90','95']
for ratio in ratios:
    with open(data_prefix + 'prior_pubmed_' + ratio, 'rb') as f:
        priors = pickle.load(f)
    with open(data_prefix + 'unlabeled_ids_pubmed_' + ratio, 'rb') as f:
        unlabeled_ids = pickle.load(f)
    with open(data_prefix + 'prediction_pubmed_' + ratio,'rb') as f:
        beliefs = pickle.load(f)
    with open(data_prefix + 'messages_pubmed_' + ratio, 'rb') as f:
        messages = pickle.load(f)
        
    beam_subgraph = {}
 
    # Iterative all review nodes and get a path with 5 nodes at most     
    for it, paper_id in enumerate(unlabeled_ids):
        if it % int(0.1*len(unlabeled_ids)) == 0:
            print('%.2f' % (it/len(unlabeled_ids)))
        if paper_id not in messages.keys():
            continue
        current_subgraph_m = [[(paper_id, paper_id)] for i in range(num_beam)]
        current_alternatives = []
        new_id = paper_id
        for i in range(max_len-1):
            current_alternatives_m = get_action_candidates(current_subgraph_m)
            if [] in current_alternatives_m:
                break
            current_subgraph_m = find_beam_subgraph(current_subgraph_m, current_alternatives_m, paper_id, num_beam)
        beam_subgraph[paper_id] = current_subgraph_m
    
    beam_combined = {}
    for paper_id, branches in beam_subgraph.items():
        temp = []
        for branch in branches:
            for node in branch:
                if node not in temp:
                    temp.append(node)
        beam_combined[paper_id] = temp
    
    
    
    with open(data_prefix + 'GEG_k' + str(num_beam) + '_pubmed_' + ratio, 'wb') as f:
        pickle.dump(beam_subgraph, f)
    with open(data_prefix + 'GEG_k' + str(num_beam) + '_combined_pubmed_' + ratio, 'wb') as f:
        pickle.dump(beam_combined, f)

