#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle
import operator
import utils


def get_neighbor_list(center, current_list = []):
    neighbors = list(messages[center].keys())
    for k in current_list :
        if k in neighbors:
            neighbors.remove(k)
    return neighbors

def candidates_value(center, neighbors):
    previous_value = {}
    for nei_node in neighbors:
        previous_value[nei_node] = utils.sum_KL(beliefs[center], messages[nei_node][center])
    previous_value[center] = utils.sum_KL(beliefs[center], priors[center])
    return previous_value

def sort_set(raw_set, k, center):
    top_k_set = {}
    sorted_set = sorted(raw_set.items(), key=operator.itemgetter(1), reverse=False)
    for i in range(min(len(raw_set),k, sorted_set.index((center, raw_set[center])))):
        top_k_set[sorted_set[i][0]] = sorted_set[i][1]
    return top_k_set


data_prefix = './data/'
ratios = ['5','10','20','30','40','50','60','70','80','90','95']
for ratio in ratios:
    with open(data_prefix + 'prediction_pubmed_' + ratio,'rb') as f:
        beliefs = pickle.load(f)
    with open(data_prefix + 'messages_pubmed_' + ratio, 'rb') as f:
        messages = pickle.load(f)
    with open(data_prefix + 'prior_pubmed_' + ratio, 'rb') as f:
        priors = pickle.load(f)
    with open(data_prefix + 'unlabeled_ids_pubmed_' + ratio, 'rb') as f:
        unlabeled_ids = pickle.load(f)
    
    #time.sleep(1999)
    budget = 4
    beam_search_nodes = {}
    
    for target_node in unlabeled_ids:    
        # Step 1
        neighbors = get_neighbor_list(target_node)
        KL_value = candidates_value(target_node, neighbors)
        top_k_set = sort_set(KL_value, budget, target_node)
        beam_search_nodes[target_node] = list(top_k_set.keys())
        if target_node not in beam_search_nodes[target_node]:
            beam_search_nodes[target_node].append(target_node)
    
    
    with open(data_prefix + 'GEL_pubmed_' + ratio, 'wb') as f:
        pickle.dump(beam_search_nodes, f)
    
    
