#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pickle
import utils
import math

def get_action_candidates(state):
    node_list = [step[1] for step in state]
    action_candidates = []
    for node in node_list:
        neighbors = list(messages[node].keys())
        for neighbor in neighbors:
            if (neighbor, node) not in state and (node, neighbor) not in state and neighbor not in node_list:
                action_candidates.append((node, neighbor))
    return action_candidates


def find_best_subgraph(cur_subgraph, cur_alter, center_id):
    if len(cur_alter) == 0:
        return 'Stop'
    best_KL = math.inf
    for pair in cur_alter:
        cand_subgraph = cur_subgraph.copy()
        cand_subgraph.append(pair)

        model = utils.BP(cand_subgraph, priors, potentials, len(potentials))
        model.schedule()
        model.run_bp()
        cand_belief = model.classify()
        cand_KL = utils.sum_KL( cand_belief[center_id], beliefs[center_id])
        if best_KL > cand_KL:
            best_KL = cand_KL
            best_alter = pair
    return best_alter

data_prefix = './data/'
ratios = ['5','10','20','30','40','50','60','70','80','90','95']
k_class = 3
eyes = (1 - 0.1*k_class/(k_class-1) ) * np.diag(np.ones(k_class))
potentials = (eyes + 0.1/(k_class-1) * np.ones([k_class, k_class]))
max_len = 5
for ratio in ratios:
    with open(data_prefix + 'prior_pubmed_' + ratio, 'rb') as f:
        priors = pickle.load(f)
    with open(data_prefix + 'unlabeled_ids_pubmed_' + ratio, 'rb') as f:
        unlabeled_ids = pickle.load(f)
    with open(data_prefix + 'prediction_pubmed_' + ratio,'rb') as f:
        beliefs = pickle.load(f)
    with open(data_prefix + 'messages_pubmed_' + ratio, 'rb') as f:
        messages = pickle.load(f)

    
    subgraph = {}
    # Iterative all review nodes and get a path with 5 nodes at most 
    for paper_id in unlabeled_ids:
        if paper_id not in messages.keys():
            continue
        current_subgraph = [(paper_id, paper_id)]
        current_alternatives = []
        new_id = paper_id
        for i in range(max_len-1):
            current_alternatives = get_action_candidates(current_subgraph)
            new_pair = find_best_subgraph(current_subgraph, current_alternatives, paper_id)
            if new_pair == 'Stop':
                break
            else:
                current_subgraph.append(new_pair)
                new_id = new_pair[1]
        subgraph[paper_id] = current_subgraph
        
    with open(data_prefix + 'GEG_pubmed_'  + ratio, 'wb') as f:
        pickle.dump(subgraph, f)

