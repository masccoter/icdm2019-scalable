#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle
import numpy as np
import time
import utils


data_prefix = './data/'

with open(data_prefix + 'paper_pair', 'rb') as f:
    paper_pair = pickle.load(f)
with open(data_prefix + 'ground_truth', 'rb') as f:
    ground_truth = pickle.load(f)

class_paper = {0:[],1:[],2:[]}
for k,v in ground_truth.items():
    class_paper[v].append(k)

# get labeled data randomly, based on the ratio between paper in specific class and total papers
np.random.seed(1)
labeled_ratio = 0.95
labeled_ids = []
for class_name, paper_list in class_paper.items():
    labeled_num = int(labeled_ratio * len(paper_list))
    labeled_ids += (np.random.choice(paper_list, labeled_num, replace = False)).tolist()
unlabeled_ids = list(set(ground_truth) - set(labeled_ids))


k_class = len(class_paper)
priors = {}
for ids in labeled_ids:
    prior = 0.1/(k_class - 1) * np.ones(k_class)
    prior[ground_truth[ids]] = 0.9
    priors[ids] = prior
for ids in unlabeled_ids:
    prior = np.ones(k_class) / k_class
    priors[ids] = prior

eyes = (1 - 0.1*k_class/(k_class-1) ) * np.diag(np.ones(k_class))
potentials = eyes + 0.1/(k_class-1) * np.ones([k_class, k_class])


model = utils.BP(paper_pair, priors, potentials, len(potentials) )
model.schedule()
model.run_bp()
belief = model.classify()
messages = model.get_messages()

ratio = str(int(labeled_ratio * 100))

with open(data_prefix + 'unlabeled_ids_pubmed_' + ratio, 'wb' ) as f:
    pickle.dump(unlabeled_ids, f)
with open(data_prefix + 'prior_pubmed_' + ratio, 'wb' ) as f:
    pickle.dump(priors, f)
with open(data_prefix + 'prediction_pubmed_' + ratio, 'wb' ) as f:
    pickle.dump(belief, f)
with open(data_prefix + 'messages_pubmed_' + ratio, 'wb' ) as f:
    pickle.dump(messages, f)





