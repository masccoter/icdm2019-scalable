#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.misc import logsumexp

def smooth(arr, eps = 1e-5):
    if 0 in arr:
        return abs(arr-eps)
    else:
        return arr

def KL_divergence(P, Q):
    # Input P and Q would be vector (like messages or priors)
    # Will calculate the KL-divergence D-KL(P || Q) = sum~i ( P(i) * log(Q(i)/P(i)) )
    # Refer to Wikipedia https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence
    P = np.array(P)
    Q = np.array(Q)
    P = smooth(P)
    Q = smooth(Q)
    return sum( P * np.log(P/Q))

def sum_KL(P,Q):
    return KL_divergence(P,Q) + KL_divergence(Q,P)


class Node(object):
    
    def __init__(self, name, prior, num_class):
        self._eps = 1e-5
        self._name = name
        self._neighbors = []
        self._prior = np.log(prior)
        self._outgoing = {}
        self._num_classes = num_class

    def add_neighbor(self, neighbor_node_id):
        self._neighbors.append(neighbor_node_id)
        self._outgoing[neighbor_node_id] = np.zeros(self._num_classes)
    
    def n_edges(self):
        return len(self._neighbors)

    def get_name(self):
        return self._name

    def get_prior(self):
        return np.exp(self._prior)

    def get_neighbors(self):
        return self._neighbors

    def get_outgoing(self):
        return self._outgoing

    def get_message_for(self, neighbor_name):
        assert neighbor_name in self._outgoing, "the neighbor %s is not a neighbor of the node %s\n" % (
        neighbor_name, self._name)
        return self._outgoing[neighbor_name]

    def get_belief(self, all_nodes):
        incoming = []

        # log 1 = 0
        belief = np.zeros(self._num_classes)

        # add log of phi
        belief += self._prior

        for node_id in self._neighbors:
            n = all_nodes[node_id]

            belief += n.get_message_for(self._name)

            incoming.append(n.get_message_for(self._name))
        return belief, incoming

    def recompute_outgoing(self, potentials, all_nodes, normalize=True):
        # return value
        diff = 0
        total, incoming = self.get_belief(all_nodes)

        # go through each neighbor of the node
        for j, n_id in enumerate(self._neighbors):
            # retrieve the actual neighboring node
            n = all_nodes[n_id]

            # log phi_i + \sum_{k~j} log m_ki
            log_m_i = total - incoming[j]


            log_H = potentials
            log_m_ij = logsumexp(log_H + np.tile(log_m_i.transpose(), (self._num_classes, 1)), axis=1)
            log_Z = logsumexp(log_H + np.tile(log_m_i.transpose(), (self._num_classes, 1)))
            log_m_ij -= log_Z
            diff += np.sum(np.abs(self._outgoing[n._name] - log_m_ij))
            self._outgoing[n._name] = log_m_ij
        return diff


class BP():
    def __init__(self, graph, priors, potentials, num_class, max_iters=1):
        self._potentials = potentials
        self._max_iters = max_iters
        self._priors = priors
        self._num_class = num_class
        
        self._nodes = {}
        self._bp_schedule = []
        
        for line in graph:
            p1_name = line[0]
            p2_name = line[1]
            if p1_name == p2_name:
                continue
            if p1_name not in self._nodes:
                self._nodes[p1_name] = Node(p1_name, self._priors[p1_name], self._num_class)
            if p2_name not in self._nodes:
                self._nodes[p2_name] = Node(p2_name, self._priors[p2_name], self._num_class)
            self._nodes[p1_name].add_neighbor(p2_name)
            self._nodes[p2_name].add_neighbor(p1_name)
            
    def output_graph(self):
        """
            output nodes, edges, priors and potentials
        """
        for n in self._nodes.values():
            print(str(n.get_name()))
            print(n.get_prior())
            print(n.get_neighbors())
            
    def schedule(self, schedule_type='bfs'):
        """ use breadth-first-search to create a BP schedule
        :param:
            schedule_type: 'bfs' or 'degree'
        :return:
        """

        # sort nodes in descending order of their degrees
        items = [(n.get_name(), n.n_edges()) for k, n in self._nodes.items()]
        items = sorted(items, key=lambda x: x[1], reverse=True)

        if schedule_type == 'degree':
            self._bp_schedule = [name for name, _ in items]
            return

        mark = set(self._nodes.keys())
        self._bp_schedule = []

        head = 0
        tail = -1

        # uncomment this for loop to get bfs + degree
        for node_id, _ in items:
            # newly-found connected component
            if node_id in mark:
                tail += 1
                self._bp_schedule.append(node_id)
                mark.remove(node_id)

                # search starting from i
                while head <= tail:
                    cur_node = self._nodes[self._bp_schedule[head]]
                    head += 1
                    for neighbor_id in cur_node._neighbors:
                        if neighbor_id in mark:
                            tail += 1
                            self._bp_schedule.append(neighbor_id)
                            mark.remove(neighbor_id)
                            
    def run_bp(self, start_iter = 0, max_iters = -1, early_stop_at = 1, tol = 1e-3):
        """ run belief propagation on the graph for MaxIters iterations
        Args:
            start_iter: continuing from the results of previous iterations
            max_iters: how many iterations to run BP. Default use the SpEagle's parameter
            early_stop_at: the percentage of nodes whose out-going messages will be updated
            tol: threshold of message differences of one iteration, below which exit BP
        Return:
            delta: the difference in messages before and after iterations of message passing
        """
        stop_at = int(len(self._bp_schedule) * early_stop_at)

        if max_iters == -1:
            max_iters = self._max_iters

        for it in range(start_iter, start_iter + max_iters, 1):
            if it % 2 == 0:
                start = stop_at - 1
                end = -1
                step = -1
            else:
                start = 0
                end = stop_at
                step = 1
            p = start
            total_updates = 0
            delta = 0
            while p != end:
                total_updates += 1
                cur_node = self._nodes[self._bp_schedule[p]]
                p += step
                delta += cur_node.recompute_outgoing(self._potentials, self._nodes)
                if total_updates > stop_at:
                    break
            delta /= total_updates
            # print('bp_iter = %d, delta = %f\n' % (it, delta))
            if abs(delta) < tol:
                break
        return delta


    def classify(self):
        posteriors = {}
        for k, n in self._nodes.items():
            # decide the type of the node and find its original name
            belief, _ = n.get_belief(self._nodes)

            # from log scale to prob scale and normalize to prob distribution
            posterior = np.exp(belief)
            posterior /= np.sum(posterior)

            posteriors[k] = posterior

        return posteriors
    
    def get_messages(self):
        messages = {}
        for k,n in self._nodes.items():
            temp = {}
            for ids in n.get_neighbors():
                temp[ids] = np.exp(n.get_message_for(ids))
            messages[k] = temp
        return messages